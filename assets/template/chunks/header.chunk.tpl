<header>
	<div class="container">
		<div class="row align-items-center">
			<div class="col-12 col-md-3 col-lg-2 text-center">
				<div class="logo-wrapper">
					<div class="light-block"></div>
					<img src="assets/components/template/image/logo-header.png" alt="" class="logo-header">
				</div>
			</div>
			<div class="col-12 col-sm-auto text-center text-sm-left">
				<span class="slogan-header">ведущий производитель</span>
				<span class="slogan-header">полимерных покрытий</span>
			</div>
			<div class="d-none d-lg-block col-sm-auto ml-auto text-center text-sm-left">
				<a href="#where-buy-lg" class="buy-btn">где купить?</a>
			</div>
			<div class="col-12 col-sm-auto ml-auto text-center text-sm-left d-lg-none">
				<a href="#where-buy-xs" class="buy-btn">где купить?</a>
			</div>
			<div class="col-12 col-md-auto ml-auto ml-lg-0 text-center mt-3 mt-md-0">
			    <a href="mailto:7105@esp.ru" class="phone">7105@esp.ru</a>
			    </div>
			    <div class="col-12 col-md-auto ml-auto ml-lg-0 text-center mt-3 mt-md-0">
				<a href="tel:+74994506739" class="phone">+7 (495) 995-80-45</a>
			</div>
		</div>
	</div>
</header>