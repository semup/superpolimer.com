<div class="block3">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-8 order-2 order-lg-1">
				<div id="carousel-block-3" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#carousel-block-3" data-slide-to="0" class="active"></li>
						<li data-target="#carousel-block-3" data-slide-to="1"></li>
					</ol>
					<div class="carousel-inner">
						<div class="carousel-item active">
							<iframe src="https://www.youtube.com/embed/ak-L-VpYPGQ?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" class="video-frame" allowfullscreen></iframe>
						</div>
						<div class="carousel-item">
							<iframe src="https://www.youtube.com/embed/Z4sTqYzukZI" frameborder="0" class="video-frame" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
					<a class="carousel-control-prev" href="#carousel-block-3" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carousel-block-3" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
			<div class="col-12 col-lg-4 order-1 order-lg-2">
				<div class="white-block2">
				    <div class="white-block-head">
						[[*head-r-block2]]
					</div>
				    [[!getImageList?
        			&tvname=`head-text-block2`
        			&tpl=`tplItem4`]]
				</div>
			</div>
		</div>
	</div>
</div>