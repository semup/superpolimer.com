<div class="block4">
	<div class="container">
		<div class="row">
			<div class="d-none d-lg-block col-3">
				<div class="bottom-logo">
					<img src="assets/components/template/image/logo-footer.png" alt="">
				</div>
			</div>
			<div class="col-12 d-md-12 col-lg-5">
				<div class="block4-head">[[*field_header_manual]]</div>
				[[!getImageList?
        			&tvname=`field_manual`
        			&tpl=`tplItem5`]]
			</div>
        			<div class="d-none d-lg-block col-lg-4">
        				<div class="block4-head" id="where-buy-lg">[[*head_where_buy]]</div>
        				[[!getImageList?
                			&tvname=`content_where_buy`
                			&tpl=`tplItem6`]]
        			</div>
        			
			<div class="col-xs-12"></div>
			<div class="d-none d-lg-block col-lg-2"></div>
			<div class="col-xs-12 col-lg-8 mt-5">
			    <iframe src="https://www.youtube.com/embed/Z4sTqYzukZI" frameborder="0" class="video-frame" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
			
        			<div class="col-12 d-lg-none">
        				<div class="block4-head" id="where-buy-xs">[[*head_where_buy]]</div>
        				[[!getImageList?
                			&tvname=`content_where_buy`
                			&tpl=`tplItem6`]]
        			</div>
		</div>
	</div>
</div>