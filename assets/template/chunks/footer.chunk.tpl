<footer>
	<div class="container">
		<div class="row align-items-center">
			<div class="col-12 col-md-auto text-center text-md-left">
				<a href="tel:+74959958045" class="phone">+7 (495) 995-80-45</a>
			</div>
			<div class="col-12 col-md-auto col-xl-3 text-center">
				<a href="mailto:7105@esp.ru" class="phone">7105@esp.ru</a>
			</div>
			<div class="col-12 col-md-auto ml-auto ml-lg-0 text-center text-md-left">
				<div class="adress">[[*adress]]</div>
			</div>
			<div class="col-auto mx-auto mx-lg-0 ml-lg-auto">
				<a href="http://semup.pro/" target="_blank" id="semUP">Разработка сайта - SEMup</a>
			</div>
		</div>
	</div>
</footer>